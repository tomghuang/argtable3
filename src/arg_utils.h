/*******************************************************************************
 * This file is part of the argtable3 library.
 *
 * Copyright (C) 2013 Tom G. Huang
 * <tomghuang@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of STEWART HEITMANN nor the  names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL STEWART HEITMANN BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef ARG_UTILS_H
#define ARG_UTILS_H

#include <stdlib.h>

#define ARG_ENABLE_TRACE 0
#define ARG_ENABLE_LOG 1

#ifdef __cplusplus
extern "C" {
#endif

enum
{
    ARG_ERR_MINCOUNT = 1,
    ARG_ERR_MAXCOUNT,
    ARG_ERR_BADINT,
    ARG_ERR_OVERFLOW,
    ARG_ERR_BADDOUBLE,
    ARG_ERR_BADDATE,
    ARG_ERR_REGNOMATCH
};

typedef void (arg_panicfn)(const char *fmt, ...);


#if defined(_MSC_VER)
#define ARG_TRACE(x) \
    __pragma(warning(push)) \
    __pragma(warning(disable:4127)) \
    do { if (ARG_ENABLE_TRACE) dbg_printf x; } while (0) \
    __pragma(warning(pop))

#define ARG_LOG(x) \
    __pragma(warning(push)) \
    __pragma(warning(disable:4127)) \
    do { if (ARG_ENABLE_LOG) dbg_printf x; } while (0) \
    __pragma(warning(pop))
#else
#define ARG_TRACE(x) \
    do { if (ARG_ENABLE_TRACE) dbg_printf x; } while (0)

#define ARG_LOG(x) \
    do { if (ARG_ENABLE_LOG) dbg_printf x; } while (0)
#endif 

extern void dbg_printf(const char *fmt, ...);
extern void arg_set_panic(arg_panicfn *proc);
extern void* xmalloc(size_t size);
extern void* xcalloc(size_t count, size_t size);
extern void* xrealloc(void* ptr, size_t size);
extern void xfree(void* ptr);

#ifdef __cplusplus
}
#endif

#endif

